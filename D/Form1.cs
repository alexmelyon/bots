﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Creatures;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
     {
        List<DotBegin> Starts = new List<DotBegin>();
        List<Creature> Creatures = new List<Creature>();
        int r = 30;
        Creature CheckedCreature;

        public Form1()
        {
            InitializeComponent();
            //pictureBox1.Width = this.Width - 158;
            //pictureBox1.Height = this.Height - 108;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gf = e.Graphics;
            gf.FillRectangle(new SolidBrush(Color.White), 0, 0, pictureBox1.Width, pictureBox1.Height);
            foreach (DotBegin N in Starts)
                N.paint(e.Graphics);
            foreach (Creature N in Creatures)
            {
                N.Paint(gf);
            }
            for (int i = 0; i < Creature.ListSize - 1; i++)
            {
                gf.DrawString(Creature.loops[i], new Font("Courier New", 8), new SolidBrush(Color.Black), 0, i * 8);
            }
            listBox1.Items.Clear();
            foreach (Creature N in Creatures)
                listBox1.Items.Add(N);
            if(CheckedCreature!=null)
            gf.DrawRectangle(new Pen(Color.Red,5), CheckedCreature.x - CheckedCreature.r, CheckedCreature.y - CheckedCreature.r, CheckedCreature.r * 2, CheckedCreature.r * 2);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            //pictureBox1.Width = this.Width - 158;
            //pictureBox1.Height = this.Height - 108;
        }

        bool GetPoint(int x, int y,out int Dot)
        {
            Dot = -1;
            for(int i=0;i<Starts.Count;i++)
            {
                if (Math.Sqrt(Math.Pow((Starts[i].x - x), 2) + Math.Pow(Starts[i].y - y, 2)) < r)
                { Dot = i; return false; }
            }
            return true;
        }
        bool GetRobot(int x,int y,out int Robot)
        {
            Robot = -1;
            for(int i=0;i<Creatures.Count;i++)
            {
                if (Math.Sqrt(Math.Pow((Creatures[i].x - x), 2) + Math.Pow(Creatures[i].y - y, 2)) < Creatures[i].r)
                { Robot = i; return false; }
            }
            return true;
        }
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            //int Dot;
            //if (e.Button == MouseButtons.Left)
            //{
            //    if (GetPoint(e.X, e.Y, out Dot))
            //    {
            //        //Starts.Add(new DotBegin(e.X, e.Y, r));
            //        //Starts[Starts.Count - 1].name = (Starts.Count - 1).ToString();
            //        Creatures.Add(new Robot1(e.X, e.Y));
            //        Creatures[Creatures.Count - 1].Name = (Creatures.Count - 1).ToString();
            //    }
            //}
            //else
            //{
            //    if (e.Button == MouseButtons.Right)
            //    {
            //        bool u = GetPoint(e.X, e.Y, out Dot);
            //        if (Dot != -1)
            //            //Starts.RemoveAt(Dot);
            //            Creatures.RemoveAt(Dot);
            //    }
            //}
            int Robot;
            if (e.Button == MouseButtons.Left)
            {
                if (GetRobot(e.X, e.Y, out Robot))
                {
                    if (toolStripComboBox1.Text == "Monopod")
                        Creatures.Add(new Monopod(e.X, e.Y));
                    if (toolStripComboBox1.Text == "Quadropod")
                        Creatures.Add(new Quadropod(e.X, e.Y));
                    CheckedCreature = Creatures[Creatures.Count - 1];
                }
                else
                {
                    CheckedCreature = Creatures[Robot];
                }
            }
            else
            {
                if (e.Button == MouseButtons.Right)
                {
                    bool u = GetRobot(e.X, e.Y, out Robot);
                    if (Robot != -1)
                        Creatures.RemoveAt(Robot);
                }
            }
            pictureBox1.Invalidate();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Creature N in Creatures)
                N.Action();
            pictureBox1.Invalidate();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            timer1.Interval = 100;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Starts.Clear();
            Creatures.Clear();
            for (int i = 0; i < Creature.ListSize; i++)
                Creature.loops[i] = "";
            pictureBox1.Invalidate();
        }

        private void stepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Creature N in Creatures)
                N.Action();
            pictureBox1.Invalidate();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

}
