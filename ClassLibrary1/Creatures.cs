﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Creatures
{
    public class DotBegin
    {
        public string name = "";
        public int x = 0;
        public int y = 0;
        public int r = 0;
        public DotBegin(int x, int y,int r)
        {
            this.x = x;
            this.y = y;
            this.r = r;
        }
        public void paint(Graphics gf) 
        {
            gf.FillEllipse(new SolidBrush(Color.Red), x-r/2, y-r/2, r,r);
            gf.DrawEllipse(new Pen(Color.Black), x - r / 2, y - r / 2, r, r);
        }
    }

    public class Leg
    {
        public int x = 0, y = 0, dx = 0, dy = 0, r = 0;
        public bool Push = false;
        public double angle = 0;
        public double dangle = 0;
        public double Length = 0;
        public int width = 1;
        public double Speed = 0;
        public Leg(int X, int Y)
        {
            this.x = X;
            this.y = Y;
            this.r = 30;
        }
        public Leg(int x, int y, int width, double angle)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.angle = angle;
        }
        public void Paint(int X,int Y,Graphics gf)
        {
            if (width == 1)
            {
                gf.FillEllipse((this.Push==false)?(new SolidBrush(Color.White)):(new SolidBrush(Color.Gray)), this.x - r / 2, this.y - r / 2, r, r);
                gf.DrawEllipse(new Pen(Color.Black), this.x - r / 2, this.y - r / 2, r, r);
                gf.DrawLine(new Pen(Color.Black, 3), this.x, this.y, X, Y);
            }

            //или если гусеница width>1
        }
    }
    public class Creature
    {
        public static int ListSize = 50;
        public static string[] loops = new string[ListSize];
        public static string Loop
        {
            set
            {
                for (int i = 0; i < ListSize - 1; i++)
                {
                    loops[i] = "";  //MessageBox.Show(i.ToString());
                    loops[i] = loops[i + 1];
                }
                loops[ListSize - 1] = value;
            }
        }
        public string[] Log;
        public string Name = "";
        public int x, y, r;
        public double angle
        {
            get
            {
                return angle;
            }
            set
            {
                angle = (angle + value) % 360;
            }
        }
        public Color color;
        public List<Leg> Legs = new List<Leg>();
        public Creature(int X, int Y,int Rad)
        {
            this.x = X;
            this.y = Y;
            this.r = Rad;
            Random Rand = new Random();
            this.Name = Rand.Next(100).ToString();
            byte R=(byte)(Rand.Next()), G=(byte)(Rand.Next()), B=(byte)(Rand.Next());
            this.color = Color.FromArgb(R, G, B);
        }
        public void Paint(Graphics gf)
        {
            gf.FillEllipse(new SolidBrush(this.color), this.x - r / 2, this.y - r / 2, r, r);
            foreach (Leg N in Legs)
                N.Paint(this.x, this.y, gf);
            gf.DrawString(this.Name+"\n"+Legs[0].angle.ToString(), new Font("Courier New", 12,FontStyle.Bold), new SolidBrush(Color.FromArgb(255-color.R,255-color.G,255-color.B)), this.x-r/4, this.y-r/4);
        }
        public virtual void Logic() { }
        public void Action()
        {
            this.Logic();
            foreach (Leg N in Legs)
            {
                N.angle += N.dangle;
                N.x += N.dx;
                N.y += N.dy;
                N.dx = (this.x + Convert.ToInt32(N.Length * Math.Cos(N.angle * 2 * Math.PI / 360))) - N.x;
                N.dy = (this.y - Convert.ToInt32(N.Length * Math.Sin(N.angle * 2 * Math.PI / 360))) - N.y;
                //N.x = this.x + Convert.ToInt32(N.Length * Math.Cos(N.angle * 2 * Math.PI / 360));
                //N.y = this.y - Convert.ToInt32(N.Length * Math.Sin(N.angle * 2 * Math.PI / 360));
                //MessageBox.Show(N.Length.ToString());
            }

            int dx = 0, dy = 0, d = 0;
            foreach (Leg N in this.Legs)
                if (N.Push == true)
                {
                    dx += N.dx;
                    dy += N.dy;
                    d++;
                }
            if (d > 0)
            {
                dx = dx / d;
                dy = dy / d;
                Loop = dx.ToString() + " " + dy.ToString();
                this.x += -dx;
                this.y += -dy;
            }

        }
        public override string ToString()
        {
            return this.Name;
        }
    }
    public class Quadropod:Creature
    {
        public Quadropod(int X, int Y)
            : base(X, Y, 50)
        {
            this.Legs.Add(new Leg(x - r, y - r));
            this.Legs.Add(new Leg(x + r, y - r));
            this.Legs.Add(new Leg(x - r, y + r));
            this.Legs.Add(new Leg(x + r, y + r));
        }
        public override void Logic()
        {

        }
    }
    public class Monopod : Creature
    {
        public Monopod(int X, int Y)
            : base(X, Y, 50)
        {
            this.Legs.Add(new Leg(X + r, Y));
            Legs[0].Length = Math.Sqrt(Math.Pow(X - Legs[0].x, 2) + Math.Pow(Y - Legs[0].y, 2));
        }
        bool forward = true;
        public override void Logic()
        {
            if (Legs[0].angle >= 60) { forward = false; Legs[0].Push = true; }
            if (Legs[0].angle <= -60) { forward = true; Legs[0].Push = false; }
            if (forward) Legs[0].dangle = 10;
            else Legs[0].dangle = -10;
        }
    }
}
